---

原文は、[The Common Lisp Cookbook – Packages](http://lispcookbook.github.io/cl-cookbook/packages.html)です。g000001さんからご助言をいただき、`do-symbols`マクロについて加筆しました。

---

# パッケージにあるシンボルを列挙する

Common Lispには、パッケージ内のシンボルをまとめて把握するためのマクロがいくつかあります。特に興味深いのは、[`do-symbols`と`do-external-symbols`][do-sym]です。

`do-symbols`は、 該当するパッケージでアクセスできる全てのシンボルを列挙します。例えば、:clパッケージにある全てのシンボルを表示するには、次のように書きます。

~~~lisp
(do-symbols (s :cl) (print s))
~~~

`DO-EXTERNAL-SYMBOLS`は、exportされたシンボルだけを列挙します。"PACKAGE"という名前のパッケージでexportされた全てのシンボルを表示するには、次のように書きます。

~~~lisp
(do-external-symbols (s (find-package "PACKAGE"))
  (print s))
~~~

また、リスト形式でシンボルを集めて表示するには、次のように書きます。

~~~lisp
(let (symbols)
  (do-external-symbols (s (find-package "PACKAGE"))
    (push s symbols))
  symbols)
~~~

もしくは、[`LOOP`][loop]マクロを使って、次のように書くこともできます。

~~~lisp
(loop for s being the external-symbols of (find-package "PACKAGE")
  collect s)
~~~

[guide]: http://www.flownet.com/gat/packages.pdf
[do-sym]: http://www.lispworks.com/documentation/HyperSpec/Body/m_do_sym.htm
[loop]: http://www.lispworks.com/documentation/HyperSpec/Body/06_a.htm
