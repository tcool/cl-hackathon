; written by KeenS
; forked from https://gist.github.com/KeenS/f3deeca6295d7454cb17

(ql:quickload :parenscript)
(ql:quickload :cl-who)
(ql:quickload :clack)
(in-package :ps)
(defvar *canvas-id* "alien-canvas")
(clack:clackup
 (lambda (env)
   (list 200
         '(:content-type "text/html")
         (list
          (who:with-html-output-to-string (*standard-output* nil :prologue t :indent t)
            (:html
             (:head
              (:script  :type "text/javascript"
               (who:fmt "~A"
                        (ps (defvar x 0)
                            (defvar y 0)
                            (defvar dx 1)
                            (defvar dy 1)
                            (defvar img (new -image))
                            (setf (@ img src) "http://www.lisperati.com/lisplogo_alien_128.png")
                            (set-interval "draw()" 5)

                            (defun draw ()
                              (let ((w 128)
                                    (h 75)
                                    (canvas ((@ document get-element-by-id) #.*canvas-id*)))
                                (if (or (not canvas) (not (@ canvas get-context)))
                                    (return false))
                                (let ((ctx ((@ canvas get-context) "2d")))
                                  ((@ ctx fill-rect) 0 0 500 500)
                                  (if (and (<= (+ x dx w) 500) (<= 0 (+ x dx)))
                                      (setf x (+ x dx))
                                      (setf dx (* dx -1)))
                                  (if (and (<= (+ y dy h) 500) (<= 0 (+ y dy)))
                                      (setf y (+ y dy))
                                      (setf dy (* dy -1)))
                                  ((@ ctx draw-image) img x y))))))))
             (:body (:canvas :id *canvas-id* :width 500 :height 500))))))))
