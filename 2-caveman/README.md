# <u>Libraries used in Common Lisp Seminar</u>

## <u>Web環境</u>

### clack

[clack](https://github.com/fukamachi/clack)は、Common LispのためのWeb開発環境です。Clackを使うと、Hunchentoot, FastCGI, Woo等のWebサーバを、同じAPIで利用することができます。

### lack
Lackを使うと、Webアプリケーションをモジュラーコンポーネントで構成できます。
元々、LackはClackの一部分でしたが、Clack(v2)がパフォーマンスと簡潔さを重視するので、別のプロジェクトとして書き直す予定です。
扱う範囲としては、Lackアプリケーションを定義して、Lackミドルウェアでラップします。
一方、Clackは、HTTP/HTTPサーバを抽象化して、統一したAPIを提供します。

## <u>ユーティリティー</u>

### Alexandria

Alexandriaは、Common Lispのユーティリティライブラリです。パブリックドメインで配布されています。

## <u>テンプレートエンジン</u>

### Djula

Caveman2のデフォルトのテンプレートエンジンです。DjangoのテンプレートエンジンをCommon Lispに移植したものです。テンプレートファイル内で {{ lisp|hoge }}のように使用します。

### cl-who

テンプレートエンジンです。HTMLをS式で表現する記法です。

### cl-bootstrap

Twitter Bootstrapを、Common Lispでウィジェットとして使うためのCommon Lispライブラリです。

## <u>データベース</u>

### SxQL

SQLを生成するDSLです。

### cl-dbi

Cl-dbiは、様々な開発段階でデータベースを使い分けるときに便利です。例えば、developmentではSQLite3を用いて、productionではMySQLを使うといったことができます。データベースの切り替えは、dbi:connectに渡す実引数を変えるだけで可能です。

### Datafly

CL-DBIのラッパーです。主要な関数としては、`retrieve-one`、`retrieve-all`、`execute`があります。それぞれは[SxQL](https://github.com/fukamachi/sxql)の記法を受け取ります。

### Mito

O/Rマッパーです。MySQL、PostgreSQL、SQLite3をサポートしています。RubyのActiveRecordのように、id (serial primary key)、created_at、updated_atがデフォルトで追加されます。マイグレーションやDBスキーマのバージョン管理もできます。開発はアルファ段階なので、APIは変更する可能性が高いので要注意とのことです。

## <u>HTTPクライアント</u>

### Dexador

HTTPクライアントです。Drakmaを置き換えるたえに開発されています。

## URIハンドリング

### Myway

My Wayは、Sinatraに相当するルーティングのためのライブラリです。元々は、Clackの`Clack.Util.Route`として書かれていました。

### Quri

Quriは、URIライブラリです。UTF-8をサポートしていて、動作は高速です。

## <u>json</u>

### jonathan

JSONのエンコーダー、デコーダーです。

## <u>Javascript</u>

### Parenscript

Common LispからJavaScriptへのコンパイラです。ブラウザではJavaScriptとして、サーバではCommon Lispとして動作します。

Parenscriptのコードは、Common Lispのコードの同じように扱われます。Lispのマクロの力をJavaScriptに使えるので、Web開発者は、コードの重複を削減し、メタプログラミングを使うことができるます。

### JSCL

Common LispからJavaScriptへのコンパイラです。

## <u>プロジェクト生成</u>

### cl-project

モダンなプロジェクトの骨組みを生成します。

## <u>暗号</u>

### Ironclad

暗号化のためのライブラリです。


